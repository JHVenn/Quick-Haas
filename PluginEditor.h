#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED
#define KNOBC 28, 71, 123

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "aboutWindow.h"

class OtherLookAndFeel : public LookAndFeel_V3
{
public:
	OtherLookAndFeel()
	{
		setColour(Slider::rotarySliderFillColourId, Colours::red);
	}

	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override
	{
		//...
		const float radius = jmin(width / 2, height / 2) - 4.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);

		// fill
		g.setColour(Colour(KNOBC));
		g.fillEllipse(rx, ry, rw, rw);
		// outline
		g.setColour(Colours::grey);
		g.drawEllipse(rx, ry, rw, rw, 3.0f);

		Path p;
		const float pointerLength = radius * 0.66f;
		const float pointerThickness = 7.0f;
		p.addRectangle(-pointerThickness * 0.5f, -radius, pointerThickness, pointerLength);
		p.applyTransform(AffineTransform::rotation(angle).translated(centreX, centreY));

		g.setColour(Colours::white);
		g.fillPath(p);
	}

	void drawToggleButton(Graphics& g, ToggleButton& button, bool isMouseOverButton, bool isButtonDown) override
	{
		const int tickWidth = jmin(20, button.getHeight() - 4);

		drawTickBox(g, button, 4.0f, (button.getHeight() - tickWidth) * 0.5f,
			(float)tickWidth, (float)tickWidth,
			button.getToggleState(),
			button.isEnabled(),
			isMouseOverButton,
			isButtonDown);

		/* g.setColour(button.findColour(ToggleButton::textColourId));
		g.setFont(jmin(15.0f, button.getHeight() * 0.6f));

		if (!button.isEnabled())
			g.setOpacity(0.5f);

		const int textX = tickWidth + 5;

		g.drawFittedText(button.getButtonText(),
			textX, 4,
			button.getWidth() - textX - 2, button.getHeight() - 8,
			Justification::centredLeft, 10); */
	}

	void drawTickBox(Graphics& g, Component& button,
		float x, float y, float w, float h,
		const bool ticked,
		const bool isEnabled,
		const bool /*isMouseOverButton*/,
		const bool isButtonDown) override
	{
		Path box;
		box.addRoundedRectangle(1.0f, 1.0f, button.getWidth() - 1.0f, button.getHeight() - 1.0f, 7.0f);

		g.setColour(isEnabled ? Colours::blue.withAlpha(isButtonDown ? 0.3f : 0.1f)
			: Colours::blue.withAlpha(isButtonDown ? 0.3f : 0.1f));

		//AffineTransform trans(AffineTransform::scale(w / 9.0f, h / 9.0f));

		g.fillPath(box);

		g.setColour(Colours::black.withAlpha(0.9f));
		//g.strokePath(box, PathStrokeType(0.9f), trans);

		if (ticked)
		{
			//g.get
			g.fillEllipse(button.getWidth()/2.0f - 5.0f, button.getHeight()/2.0f - 5.0f, 12.0f, 12.0f);

			//g.strokePath(tick, PathStrokeType(2.5f), trans);
		}
	}

};

class EmbeddedFonts
{
private:
	Font lato;
	Font latoBold;

public:
	EmbeddedFonts();
	Font& getLato();
	Font& getLatoBold();
};

class QuickHaasAudioProcessorEditor  : public AudioProcessorEditor, private SliderListener, private ButtonListener, private Timer
{
public:
	QuickHaasAudioProcessorEditor (QuickHaasAudioProcessor&);
	~QuickHaasAudioProcessorEditor();

    //==============================================================================
	void paint (Graphics&) override;
	void resized() override;

	void sliderValueChanged(Slider* slider) override;
	void buttonClicked(juce::Button * button) override;

	void timerCallback() override;

private:
	// This reference is provided as a quick way for your editor to
	// access the processor object that created it.
	QuickHaasAudioProcessor& processor;

	Slider haasPan;

	ToggleButton landR;
	ToggleButton justL;
	ToggleButton justR;
	ToggleButton mono;

	Label labelL;
	Label labelR;
	Label labelBoth;
	Label lmono;

	Label lPan;

	Image vlogo;

	OtherLookAndFeel otherLookAndFeel;

	int vertShift = 5;
	int horShift = 25;

	void setHaasDelay(float value, bool updateProcessor);

	float editorValue = 0.0f;

	void initButtons();

	EmbeddedFonts myFonts;

	ScopedPointer<aboutWindow> about;

	TextButton aboutButton;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (QuickHaasAudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
