#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
QuickHaasAudioProcessor::QuickHaasAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
	addParameter(hdelay = new AudioParameterFloat("hdelay", "Haas Delay", -0.019f, 0.019f, 0.0f));
	addParameter(stereoType = new AudioParameterInt("stereoType", "Stereo Type", 0, 3, 0));
	delayStreamL = DelayStream();
	delayStreamR = DelayStream();
}

QuickHaasAudioProcessor::~QuickHaasAudioProcessor()
{
}

//==============================================================================
const String QuickHaasAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool QuickHaasAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool QuickHaasAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double QuickHaasAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int QuickHaasAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int QuickHaasAudioProcessor::getCurrentProgram()
{
    return 0;
}

void QuickHaasAudioProcessor::setCurrentProgram (int index)
{
}

const String QuickHaasAudioProcessor::getProgramName (int index)
{
    return String();
}

void QuickHaasAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void QuickHaasAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
	delayStreamL.initSize((int)sampleRate * 2.0);
	delayStreamR.initSize((int)sampleRate * 2.0);

	currentSampleRate = sampleRate;
	if (*hdelay == 0)
	{
		delayStreamL.setDelayTime(*hdelay, sampleRate);
		delayStreamR.setDelayTime(*hdelay, sampleRate);
	}
	else if (*hdelay < 0)
	{
		delayStreamL.setDelayTime(0.0, sampleRate);
		delayStreamR.setDelayTime(*hdelay, sampleRate);
	}
	else if (*hdelay > 0)
	{
		delayStreamL.setDelayTime(*hdelay, sampleRate);
		delayStreamR.setDelayTime(0.0, sampleRate);
	}
}

void QuickHaasAudioProcessor::releaseResources()
{
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool QuickHaasAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
	if (layouts.getMainOutputChannels() <= 2 && layouts.getMainOutputChannels() == 2)
	{
		return true;
	}
	
	return false;
}
#endif

void QuickHaasAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
	const int totalNumInputChannels  = getTotalNumInputChannels();
	const int totalNumOutputChannels = getTotalNumOutputChannels();

	if (totalNumInputChannels == 1 || *stereoType == 1)
	{
		buffer.copyFrom(1, 0, buffer.getReadPointer(0), buffer.getNumSamples());
	}
	else if (*stereoType == 2)
	{
		buffer.copyFrom(0, 0, buffer.getReadPointer(1), buffer.getNumSamples());
	}
	else if (*stereoType == 3)
	{
		convertMono(buffer);
	}

	const int numChans = buffer.getNumChannels();

	if (delayChanged || *hdelay != oldDelay)
	{
		if (*hdelay == 0)
		{
			delayStreamL.setDelayTime(*hdelay, currentSampleRate);
			delayStreamR.setDelayTime(*hdelay, currentSampleRate);
		}
		else if (*hdelay < 0)
		{
			delayStreamL.setDelayTime(0.0, currentSampleRate);
			delayStreamR.setDelayTime(*hdelay, currentSampleRate);
		}
		else if (*hdelay > 0)
		{
			delayStreamL.setDelayTime(*hdelay, currentSampleRate);
			delayStreamR.setDelayTime(0.0, currentSampleRate);
		}
		delayChanged = false;
	}

	if (numChans == 2)
	{
		delayChannel(0, buffer, delayStreamL);
		delayChannel(1, buffer, delayStreamR);
	}

	oldDelay = *hdelay;
}

//==============================================================================
bool QuickHaasAudioProcessor::hasEditor() const
{
    return true;
}

AudioProcessorEditor* QuickHaasAudioProcessor::createEditor()
{
    return new QuickHaasAudioProcessorEditor (*this);
}

//==============================================================================
void QuickHaasAudioProcessor::getStateInformation (MemoryBlock& destData)
{
	ScopedPointer<XmlElement> xml(new XmlElement("PluginSettings"));
	xml->setAttribute("hdelayf", (double)*hdelay);
	xml->setAttribute("stereoType", (int)*stereoType);

	copyXmlToBinary(*xml, destData);

}

void QuickHaasAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
	ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
	if (xmlState != nullptr)
		if (xmlState->hasTagName("PluginSettings"))
		{
			*hdelay = xmlState->getDoubleAttribute("hdelayf", 0.0);
			*stereoType = xmlState->getIntAttribute("stereoType", 0);
		}
	settingsLoaded = true;

}

void QuickHaasAudioProcessor::delayChannel(int channel, AudioSampleBuffer &outBuffer, DelayStream &delayStream)
{
	int dpr, dpw;
	int olddpr = delayStream.prevDelayrd;
	int incomingSize = outBuffer.getNumSamples();

	float* channelData = outBuffer.getWritePointer(channel);
	float* delayData = delayStream.delayBuffer.getWritePointer(channel);

	dpr = delayStream.delayrd;
	dpw = delayStream.delaywrt;

	if (dpr != olddpr) {
		for (int i = 0; i < incomingSize; i++)
		{
			const float in = channelData[i];
			float out = 0.0;
			float oldout = 0.0;

			if (dpr != dpw)
				out = delayData[dpr];
			else 
				out = in;

			if (olddpr != dpw)
				oldout = delayData[olddpr];
			else
				oldout = in;

			delayData[dpw] = in;

			if (++dpr >= delayStream.delayBufferSize)
				dpr = 0;
			if (++olddpr >= delayStream.delayBufferSize)
				olddpr = 0;
			if (++dpw >= delayStream.delayBufferSize)
				dpw = 0;

			channelData[i] = out * static_cast<float>(i) / incomingSize + oldout * (1.0 - static_cast<float>(i) / incomingSize);
		}
	}
	else {
		for (int i = 0; i < incomingSize; i++)
		{
			const float in = channelData[i];
			float out = 0.0;

			if (dpr != dpw)
				out = delayData[dpr];
			else 
				out = in;

			delayData[dpw] = in;

			if (++dpr >= delayStream.delayBufferSize)
				dpr = 0;
			if (++dpw >= delayStream.delayBufferSize)
				dpw = 0;

			channelData[i] = out;
		}
	}

	delayStream.delayrd = dpr;
	delayStream.delaywrt = dpw;
	delayStream.prevDelayrd = dpr;

}

void QuickHaasAudioProcessor::convertMono(AudioSampleBuffer & buffer)
{
	if (buffer.getNumChannels() != 2)
		return;
	float* channelDataL = buffer.getWritePointer(0);
	float* channelDataR = buffer.getWritePointer(1);

	for (int i = 0; i < buffer.getNumSamples(); i++)
	{
		float avgSum = 0.5f*channelDataL[i] + 0.5f*channelDataR[i];
		channelDataL[i] = avgSum;
		channelDataR[i] = avgSum;
	}
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new QuickHaasAudioProcessor();
}

void DelayStream::setDelayTime(float time, double sampleRate)
{
	//delayBuffer.clear();

	prevDelayrd = delayrd;

	float hdelaySamples = fabs(time) * sampleRate;

	//delaywrt = 0;

	delayrd = (int)(delaywrt - hdelaySamples + delayBufferSize) % delayBufferSize;
}
