#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/**
*/

class DelayStream
{
public:
	AudioSampleBuffer delayBuffer;
	int delayrd;
	int delaywrt;
	int delayBufferSize;
	int prevDelayrd;

	DelayStream()
	{
		delayBuffer = AudioSampleBuffer(2, 0);
		delayrd = 0;
		delaywrt = 0;
		delayBufferSize = 0;
		prevDelayrd = 0;
	}

	void initSize(int size)
	{
		delayBufferSize = size;
		delayBuffer.setSize(2, size);
		delayBuffer.clear();
		delaywrt = 0;
	}

	void setDelayTime(float time, double sampleRate);

};

class QuickHaasAudioProcessor  : public AudioProcessor
{
public:
	//==============================================================================
	QuickHaasAudioProcessor();
	~QuickHaasAudioProcessor();
	
	//==============================================================================
	void prepareToPlay (double sampleRate, int samplesPerBlock) override;
	void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
	bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif
	
	void processBlock (AudioSampleBuffer&, MidiBuffer&) override;
	
	//==============================================================================
	AudioProcessorEditor* createEditor() override;
	bool hasEditor() const override;
	
	//==============================================================================
	const String getName() const override;
	
	bool acceptsMidi() const override;
	bool producesMidi() const override;
	double getTailLengthSeconds() const override;
	
	//==============================================================================
	int getNumPrograms() override;
	int getCurrentProgram() override;
	void setCurrentProgram (int index) override;
	const String getProgramName (int index) override;
	void changeProgramName (int index, const String& newName) override;
	
	//==============================================================================
	void getStateInformation (MemoryBlock& destData) override;
	void setStateInformation (const void* data, int sizeInBytes) override;

	//==============================================================================

	AudioParameterFloat *hdelay;
	int currentSampleRate;
	bool delayChanged = false;

	AudioParameterInt *stereoType;

	bool settingsLoaded = false;

private:

	DelayStream delayStreamL;
	DelayStream delayStreamR;

	void delayChannel(int channel, AudioSampleBuffer &outBuffer, DelayStream &delayStream);

	void convertMono(AudioSampleBuffer &buffer);

	float oldDelay = 0.0;



    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (QuickHaasAudioProcessor)
};


#endif  // PLUGINPROCESSOR_H_INCLUDED
