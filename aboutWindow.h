#ifndef ABOUTWINDOW_H_INCLUDED
#define ABOUTWINDOW_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

//==============================================================================
/*
*/
class aboutWindow    : public Component, private ButtonListener
{
public:

	Label aboutText;
	HyperlinkButton vennLink;
	TextButton close;
	Image vlogosmall;
	URL donate;
	ImageButton donateButton;
	Image donateImg;

    aboutWindow()
    {

		donate = URL("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=X3LSS62VREUGY");
		vlogosmall = ImageCache::getFromMemory(BinaryData::logosmall_png, BinaryData::logosmall_pngSize);
		donateImg = ImageCache::getFromMemory(BinaryData::donate_png, BinaryData::donate_pngSize);

		aboutText.setText("Quick Haas: Simple Haas Delay Pan\nProvided by: Venn Audio\nDeveloped by: Jonathan Hyde\nLicense: GNU GPL v3", NotificationType::dontSendNotification);
		aboutText.setFont(12.0f);
		aboutText.setJustificationType(Justification::topLeft);

		vennLink.setURL(URL("http://www.vennaudio.com"));
		vennLink.setButtonText("www.vennaudio.com");
		vennLink.setFont(Font("Arial", 14.0f, 0), false, Justification::left);

		close.setButtonText("Okay!");
		close.addListener(this);

		donateButton.setImages(true, true, true, donateImg, 1.0f, Colours::transparentBlack, Image(), 0.9f, Colours::transparentBlack, Image(), 0.5f, Colours::transparentBlack);
		donateButton.addListener(this);

		addAndMakeVisible(&aboutText);
		addAndMakeVisible(&vennLink);
		addAndMakeVisible(&close);
		addAndMakeVisible(&donateButton);
    }

    ~aboutWindow()
    {
    }

    void paint (Graphics& g) override
    {

        g.fillAll (Colours::white); 

        g.setColour (Colours::grey);
        g.drawRect (getLocalBounds(), 1);  

		g.drawImage(vlogosmall, 25, 100, vlogosmall.getWidth(), vlogosmall.getHeight(), 0, 0, vlogosmall.getWidth(), vlogosmall.getHeight());

    }

    void resized() override
    {
		aboutText.setBounds(10, 10, getWidth() - 10, 55);
		vennLink.setBounds(15, 55, getWidth() - 15, 25);
		close.setBounds(getWidth() - 65, getHeight() - 15 - donateButton.getHeight() + 2, 50, donateButton.getHeight() - 4);
		donateButton.setBounds(15, getHeight() - 15 - donateButton.getHeight(), donateButton.getWidth(), donateButton.getHeight());
    }

	void buttonClicked(juce::Button * button) override
	{
		if (button == &close)
			this->setVisible(false);
		else if (button == &donateButton)
			donate.launchInDefaultBrowser();
	}

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (aboutWindow)
};


#endif  // ABOUTWINDOW_H_INCLUDED
