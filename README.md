# Quick Haas

Quick Haas is a simple and streamlined haas delay pan audio effect plugin, which offers a realistic stereo panning effect by slightly delaying either the left or right channel. Quick Haas offers precision down to the one tenth of a milisecond. This effect is not mono compatible. This project is currently in beta.

### Compiling

- Quick Haas uses [Juce](https://www.juce.com/) v4+ framework. You may generate a new JUCE plugin project in Projucer, and replace the source code files with the Quick Haas source.
- If you're building on windows, you will also need the VST3 SDK, which you can download from from the [Steinberg SDK download portal](http://www.steinberg.net/nc/en/company/developers/sdk_download_portal.html).

### Credits

- Quick Haas was developed by Jonathan Hyde of [Venn Audio](http://www.vennaudio.com/) and [Small Ocean](http://www.smallocean.net)

### License
- Quick Haas is licensed under the terms of the GNU General Public License Version 3