#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "aboutWindow.h"
#include <iomanip>
#include <sstream>
#define BARC 50, 50, 50
#define BGC 210, 210, 210

//==============================================================================
QuickHaasAudioProcessorEditor::QuickHaasAudioProcessorEditor(QuickHaasAudioProcessor& p)
	: AudioProcessorEditor(&p), processor(p)
{
	vlogo = ImageCache::getFromMemory(BinaryData::vlogo_png, BinaryData::vlogo_pngSize);

	haasPan.setSliderStyle(Slider::RotaryVerticalDrag);
	haasPan.setRange(-0.019f, 0.019f, 0.0001f);
	haasPan.setName("Haas Pan");
	haasPan.setTextBoxStyle(Slider::NoTextBox, false, 90, 0);
	haasPan.setMouseCursor(MouseCursor::UpDownResizeCursor);
	haasPan.setVelocityBasedMode(false);
	haasPan.setVelocityModeParameters(0.5, 0.1, 0.01);
	haasPan.setValue(*processor.hdelay, NotificationType::dontSendNotification);
	haasPan.setDoubleClickReturnValue(true, 0.0f);
	haasPan.setLookAndFeel(&otherLookAndFeel);

	addAndMakeVisible(&haasPan);

	haasPan.addListener(this);

	initButtons();

	landR.addListener(this);
	justL.addListener(this);
	justR.addListener(this);
	mono.addListener(this);

	landR.setLookAndFeel(&otherLookAndFeel);
	justL.setLookAndFeel(&otherLookAndFeel);
	justR.setLookAndFeel(&otherLookAndFeel);
	mono.setLookAndFeel(&otherLookAndFeel);

	addAndMakeVisible(&landR);
	addAndMakeVisible(&justL);
	addAndMakeVisible(&justR);
	addAndMakeVisible(&mono);

	labelL.setFont(myFonts.getLato());
	labelR.setFont(myFonts.getLato());
	labelBoth.setFont(myFonts.getLato());
	lmono.setFont(myFonts.getLato());
	labelL.setText("Dual L", NotificationType::dontSendNotification);
	labelR.setText("Dual R", NotificationType::dontSendNotification);
	labelBoth.setText("L + R", NotificationType::dontSendNotification);
	lmono.setText("Mono", NotificationType::dontSendNotification);
	labelL.setJustificationType(Justification::centred);
	labelR.setJustificationType(Justification::centred);
	labelBoth.setJustificationType(Justification::centred);
	lmono.setJustificationType(Justification::centred);

	addAndMakeVisible(&labelBoth);
	addAndMakeVisible(&labelL);
	addAndMakeVisible(&labelR);
	addAndMakeVisible(&lmono);

	lPan.setFont(myFonts.getLato());
	lPan.setText("00.0ms C", NotificationType::dontSendNotification);
	lPan.setJustificationType(Justification::centred);
	
	addAndMakeVisible(&lPan);

	if (processor.settingsLoaded)
	{
		processor.settingsLoaded = false;
		haasPan.setValue(*processor.hdelay, NotificationType::dontSendNotification);
		initButtons();
	}

	about = new aboutWindow();

	aboutButton.setButtonText("?");
	aboutButton.addListener(this);
	addAndMakeVisible(&aboutButton);

	setSize (250, 250);
	startTimer(100);
}

QuickHaasAudioProcessorEditor::~QuickHaasAudioProcessorEditor()
{
}

//==============================================================================
void QuickHaasAudioProcessorEditor::paint (Graphics& g)
{
	g.fillAll (Colour(BGC));
	g.setColour(Colour(BARC));
	g.fillRect(0, 0, 250, 43);
	g.drawImage(vlogo, 40 + horShift, 9, 25, 25, 0, 0, vlogo.getWidth(), vlogo.getHeight());
	g.setColour (Colours::white);
	//g.setFont(Font("Arial", 15, Font::bold));
	g.setFont(myFonts.getLatoBold());
	g.setFont(16);
	g.drawFittedText ("Quick Haas", 60 + horShift, 9, 100, 25, Justification::centred, 1);
	if (processor.getNumInputChannels() < 2)
		g.setColour(Colours::grey);
	else
		g.setColour(Colours::black);
	//g.setFont(Font("Arial", 15, 0));
	g.setFont(myFonts.getLato());
	g.setFont(15);
	g.drawFittedText("Stereo Input Routing", 25 + horShift, 160 + vertShift, 150, 25, Justification::centred, 1);
}

void QuickHaasAudioProcessorEditor::resized()
{
	haasPan.setBounds(60 + horShift, 50 + vertShift, 80, 80);

	landR.setBounds(38, 190 + vertShift, 24, 24);
	justL.setBounds(88, 190 + vertShift, 24, 24);
	justR.setBounds(138, 190 + vertShift, 24, 24);
	mono.setBounds(188, 190 + vertShift, 24, 24);

	labelBoth.setBounds(25, 210 + vertShift, 50, 24);
	labelL.setBounds(75, 210 + vertShift, 50, 24);
	labelR.setBounds(125, 210 + vertShift, 50, 24);
	lmono.setBounds(175, 210 + vertShift, 50, 24);

	lPan.setBounds(60 + horShift, 130 + vertShift, 80, 24);

	if (processor.getTotalNumInputChannels() < 2)
	{
		landR.setEnabled(false);
		landR.setToggleState(false, NotificationType::dontSendNotification);
		justL.setEnabled(false);
		justL.setToggleState(false, NotificationType::dontSendNotification);
		justR.setEnabled(false);
		justR.setToggleState(false, NotificationType::dontSendNotification);
		mono.setEnabled(false);
		mono.setToggleState(false, NotificationType::dontSendNotification);
		labelBoth.setColour(Label::textColourId, Colours::grey);
		labelL.setColour(Label::textColourId, Colours::grey);
		labelR.setColour(Label::textColourId, Colours::grey);
		lmono.setColour(Label::textColourId, Colours::grey);
	}

	aboutButton.setBounds(230, 230, 15, 15);
	addChildComponent(about);

	about->setBounds(10, 10, 230, 230);
}

void QuickHaasAudioProcessorEditor::sliderValueChanged(Slider * slider)
{
	setHaasDelay(haasPan.getValue(), true);
}

void QuickHaasAudioProcessorEditor::buttonClicked(juce::Button * button)
{
	if (button == &landR)
	{
		justL.setToggleState(false, NotificationType::dontSendNotification);
		justL.setEnabled(true);
		justR.setToggleState(false, NotificationType::dontSendNotification);
		justR.setEnabled(true);
		mono.setToggleState(false, NotificationType::dontSendNotification);
		mono.setEnabled(true);
		*processor.stereoType = 0;
		landR.setEnabled(false);
	}
	else if (button == &justL)
	{
		landR.setToggleState(false, NotificationType::dontSendNotification);
		landR.setEnabled(true);
		justR.setToggleState(false, NotificationType::dontSendNotification);
		justR.setEnabled(true);
		mono.setToggleState(false, NotificationType::dontSendNotification);
		mono.setEnabled(true);
		*processor.stereoType = 1;
		justL.setEnabled(false);
	}
	else if (button == &justR)
	{
		justL.setToggleState(false, NotificationType::dontSendNotification);
		justL.setEnabled(true);
		landR.setToggleState(false, NotificationType::dontSendNotification);
		landR.setEnabled(true);
		mono.setToggleState(false, NotificationType::dontSendNotification);
		mono.setEnabled(true);
		*processor.stereoType = 2;
		justR.setEnabled(false);
	}
	else if (button == &mono)
	{
		justL.setToggleState(false, NotificationType::dontSendNotification);
		justL.setEnabled(true);
		landR.setToggleState(false, NotificationType::dontSendNotification);
		landR.setEnabled(true);
		justR.setToggleState(false, NotificationType::dontSendNotification);
		justR.setEnabled(true);
		*processor.stereoType = 3;
		mono.setEnabled(false);
	}
	else if (button == &aboutButton)
	{
		about->setVisible(true);
	}
}

void QuickHaasAudioProcessorEditor::timerCallback()
{
	if (editorValue != *processor.hdelay)
	{
		haasPan.setValue(*processor.hdelay, NotificationType::dontSendNotification);
		setHaasDelay(*processor.hdelay, false);
	}
}

void QuickHaasAudioProcessorEditor::setHaasDelay(float value, bool updateProcessor)
{
	String pan;
	std::string valText;
	float newValue = value;

	if (newValue < -0.00009)
		pan = " L";
	else if (newValue > 0.00009)
		pan = " R";
	else
	{
		pan = " C";
		newValue = 0.0f;
		//updateProcessor = true; //if automation value is insignificantly different from 0, change to 0
	}

	float newValms = fabs(newValue) * 1000.0f;

	if (updateProcessor)
	{
		*processor.hdelay = newValue;
		processor.delayChanged = true;
	}

	std::stringstream stream;
	stream << std::fixed << std::setprecision(1) << newValms;
	if (newValms < 10.0f)
		valText = "0" + stream.str();
	else
		valText = stream.str();

	lPan.setText(valText + "ms" + pan, NotificationType::dontSendNotification);

	editorValue = newValue;
}

void QuickHaasAudioProcessorEditor::initButtons()
{
	if (*processor.stereoType == 0)
	{
		justL.setToggleState(false, NotificationType::dontSendNotification);
		justL.setEnabled(true);
		justR.setToggleState(false, NotificationType::dontSendNotification);
		justR.setEnabled(true);
		mono.setToggleState(false, NotificationType::dontSendNotification);
		mono.setEnabled(true);
		landR.setToggleState(true, NotificationType::dontSendNotification);
		landR.setEnabled(false);
	}
	else if (*processor.stereoType == 1)
	{
		landR.setToggleState(false, NotificationType::dontSendNotification);
		landR.setEnabled(true);
		justR.setToggleState(false, NotificationType::dontSendNotification);
		justR.setEnabled(true);
		mono.setToggleState(false, NotificationType::dontSendNotification);
		mono.setEnabled(true);
		justL.setToggleState(true, NotificationType::dontSendNotification);
		justL.setEnabled(false);
	}
	else if (*processor.stereoType == 2)
	{
		justL.setToggleState(false, NotificationType::dontSendNotification);
		justL.setEnabled(true);
		landR.setToggleState(false, NotificationType::dontSendNotification);
		landR.setEnabled(true);
		mono.setToggleState(false, NotificationType::dontSendNotification);
		mono.setEnabled(true);
		justR.setToggleState(true, NotificationType::dontSendNotification);
		justR.setEnabled(false);
	}
	else if (*processor.stereoType == 3)
	{
		justL.setToggleState(false, NotificationType::dontSendNotification);
		justL.setEnabled(true);
		landR.setToggleState(false, NotificationType::dontSendNotification);
		landR.setEnabled(true);
		justR.setToggleState(false, NotificationType::dontSendNotification);
		justR.setEnabled(true);
		mono.setToggleState(true, NotificationType::dontSendNotification);
		mono.setEnabled(false);
	}
}


EmbeddedFonts::EmbeddedFonts()
{
	lato = Font(Typeface::createSystemTypefaceFor(BinaryData::LatoRegular_ttf,
		BinaryData::LatoRegular_ttfSize));
	
	latoBold = Font(Typeface::createSystemTypefaceFor(BinaryData::LatoBold_ttf,
		BinaryData::LatoBold_ttfSize));

}

Font& EmbeddedFonts::getLato()
{
	return lato;
}

Font& EmbeddedFonts::getLatoBold()
{
	return latoBold;
}